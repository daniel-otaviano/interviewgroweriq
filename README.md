## QA Interview

### Based on the Shopping Cart application. Create e2e (cypress) test cases that test the main functionalities present in the webservice.

## Running the Application
1. clone the project.
2. Install angular globally and build the dependencies.

```
npm install -g @angular/cli
cd interview_grower_id_qa
npm install
```
## Criteria
### Use cypress resources to create detailed tests of all the functionalities present in the application, once complete create a separate repository in bitbucket and push the changes to the main branch.
