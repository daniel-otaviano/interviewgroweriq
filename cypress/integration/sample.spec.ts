import itemsLocators from '../support/locators/itemsLocators'

describe('E2E Test complete', () => {
  beforeEach(() => {
    cy.visit('http://localhost:4200');
  });

  it('Buy Item', () => {
    cy.BuyItem()
  });

  it('Delete Item', () => {
    cy.BuyItem();
    cy.DeleteItem();
  })

  it('Validate Quantity Field Minimum', () => {
    cy.QuantityFieldMinimum();
  })

  it('Validate Quantity Field Required', () => {
    cy.QuantityFieldRequired();
  })
});
