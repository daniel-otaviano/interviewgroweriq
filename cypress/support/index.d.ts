

declare namespace Cypress {
  interface Chainable {
    sample(): Chainable<Element>;
    BuyItem(): Chainable<Element>;
    DeleteItem(): Chainable<Element>;
    QuantityFieldMinimum(): Chainable<Element>;
    QuantityFieldRequired(): Chainable<Element>;
  }
}
