let randomNumber = parseInt(Math.random() * 6);

class itemsLocators {
  //items
  static arrowOption = '.mat-select-arrow';
  static chooseOption = `#mat-option-${randomNumber} > .mat-option-text`;
  static numberOption = '#mat-input-0';
  static buttonAddCart = '.add-button';
  static totalField = '.total';
  static trashButton = '.material-icons';
  static messageError = '#mat-error-0';
  static body = 'body';
}

export default itemsLocators;
