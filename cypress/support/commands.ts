// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom

// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

import itemsLocators from '../support/locators/itemsLocators'

Cypress.Commands.add('BuyItem', () => {
    cy.get(itemsLocators.arrowOption).click();
    cy.get(itemsLocators.chooseOption).click();
    cy.get(itemsLocators.numberOption).type('5');
    cy.get(itemsLocators.buttonAddCart).click();
    cy.get(itemsLocators.totalField).should('exist');
})

Cypress.Commands.add('DeleteItem', () => {
  cy.get(itemsLocators.trashButton).click();
  cy.get(itemsLocators.totalField).contains('Total: $0.00');
})

Cypress.Commands.add('QuantityFieldMinimum', () => {
  cy.get(itemsLocators.arrowOption).click();
  cy.get(itemsLocators.chooseOption).click();
  cy.get(itemsLocators.numberOption).type('0');
  cy.get(itemsLocators.messageError).contains('Minimum quantity required, at least one.')
})

Cypress.Commands.add('QuantityFieldRequired', () => {
  cy.get(itemsLocators.arrowOption).click();
  cy.get(itemsLocators.chooseOption).click();
  cy.get(itemsLocators.numberOption).type(' ');
  cy.get(itemsLocators.body).click();
  cy.get(itemsLocators.messageError).contains('Quantity is required.')

})

