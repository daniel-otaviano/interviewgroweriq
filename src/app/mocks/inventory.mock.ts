import { Inventory } from '../interfaces/inventory.interfaces';

export const inventory: Inventory[] = [
  {
    "name": "Bread",
    "price": 3.60,
    "category": ["Dairy"],
  },
  {
    "name": "Chip",
    "price": 2.50,
    "category": ["Snack"],
  },
  {
    "name": "Milk",
    "price": 1,
    "category": ["Dairy"],
  },
  {
    "name": "Egg",
    "price": 4.50,
    "category": ["Dairy"],
  },
  {
    "name": "Apple",
    "price": 0.20,
    "category": ["Fruit"],
  },
  {
    "name": "Chocolate",
    "price": 5,
    "category": ["Dairy","Snack"],
  }
];
