export interface Inventory {
  name: string;
  price: number;
  category: string[];
}
